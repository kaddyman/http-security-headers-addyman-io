FROM amazonlinux:latest

RUN yum -y install zip \
    && yum clean all

ADD app /app

RUN cd /app && zip -r ../http-security-headers-addyman-io.zip *
