'use strict';
exports.handler = (event, context, callback) => {  
  const response = event.Records[0].cf.response;
  const headers = response.headers;

   headers['strict-transport-security'] = [{key: 'Strict-Transport-Security', value: 'max-age=63072000; includeSubdomains; preload'}]; 
   headers['content-security-policy'] = [{key: 'Content-Security-Policy', value: "default-src 'self' www.addyman.io addyman.io; connect-src https://stats.g.doubleclick.net https://www.googletagmanager.com https://www.gstatic.com https://www.google-analytics.com www.addyman.io addyman.io https://watchingyou.report-uri.com; img-src 'self' www.addyman.io addyman.io https://www.google-analytics.com www.google-analytics.com https://stats.g.doubleclick.net; script-src 'self' www.addyman.io addyman.io https://www.googletagmanager.com https://www.gstatic.com https://www.google-analytics.com; upgrade-insecure-requests; report-to default;"}] 
   headers['x-content-type-options'] = [{key: 'X-Content-Type-Options', value: 'nosniff'}]; 
   headers['x-frame-options'] = [{key: 'X-Frame-Options', value: 'DENY'}]; 
   headers['x-xss-protection'] = [{key: 'X-XSS-Protection', value: '1; mode=block; report=https://watchingyou.report-uri.com/r/d/xss/enforce'}]; 
   headers['referrer-policy'] = [{key: 'Referrer-Policy', value: 'same-origin'}]; 
   headers['feature-policy'] = [{key: 'Feature-Policy', value: "autoplay 'none'; camera 'none'"}];
   headers['report-to'] = [{key: 'Report-To', value: "{\"group\":\"default\",\"max_age\":31536000,\"endpoints\":[{\"url\":\"https://watchingyou.report-uri.com/a/d/g\"}],\"include_subdomains\":true}"}];
   headers['nel'] = [{key: 'NEL', value: "{\"report_to\":\"default\",\"max_age\":31536000,\"include_subdomains\":true}"}];
   headers['expect-ct'] = [{key: 'Expect-CT', value: "max-age=86400; enforce; report-uri=https://watchingyou.report-uri.com/r/d/ct/enforce"}]
 
  callback(null, response);
};

