# cloudfront lambda@edge http security headers

This node.js 10.x app will act as a lambda@edge for you static s3 websites 
in cloudfront. It will add just about every http security headers available. 
Watch out for content-security-policy as that will break your website 
very quickly if you don't know what you are doing. 

Use the Makefile to build/deploy lambda. 
