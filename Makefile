SHELL := /usr/bin/env bash
IMAGE_NAME="app_zipper"
VERSION="latest"
FUNCTION_NAME="http-security-headers-addyman-io"
LAMBDA_BUCKET="kac-lambdas"
REGION="us-east-1"

default: build

build:
	docker build --rm --tag $(IMAGE_NAME):$(VERSION) .
	docker run app_zipper
	docker cp `docker ps -l -q`:/$(FUNCTION_NAME).zip .


publish:
	openssl dgst -sha256 -binary $(FUNCTION_NAME).zip | openssl enc -base64 > $(FUNCTION_NAME).zip.base64sha256
	AWS_PROFILE=ken aws s3 cp $(FUNCTION_NAME).zip s3://$(LAMBDA_BUCKET)/$(FUNCTION_NAME)/$(FUNCTION_NAME).zip
	AWS_PROFILE=ken aws s3 cp $(FUNCTION_NAME).zip.base64sha256 s3://$(LAMBDA_BUCKET)/$(FUNCTION_NAME)/$(FUNCTION_NAME).zip.base64sha256 --content-type text/plain
	AWS_PROFILE=kac aws lambda update-function-code --function-name $(FUNCTION_NAME) --s3-bucket $(LAMBDA_BUCKET) --s3-key $(FUNCTION_NAME)/$(FUNCTION_NAME).zip --region $(REGION) --publish
	rm $(FUNCTION_NAME).zip*
